public class MethodsTest
{
	public static void main(String args[])
	{
		int x = 10;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x+50);
		methodTwoInputNoReturn(5, 7.25);
		int y = methodNoInputReturnInt();
		System.out.println(y);
		double returnedValue = sumSquareRoot(6, 3);
		System.out.println(returnedValue);
		String s1 = "Hello";
		String s2 = "Goodbye";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		int x = 50;
		System.out.println(x);
		System.out.println("I'm in a method that takes no imput and returns nothing.");
	}
	public static void methodOneInputNoReturn(int inputOneInt)
	{
		System.out.println("Inside this method there is one input and no return");
		System.out.println(inputOneInt);
	}
	public static void methodTwoInputNoReturn(int inputTwoInt, double inputTwoDouble)
	{
		System.out.println("This method takes in two inputs and returns nothing.");
		System.out.println(inputTwoInt);
		System.out.println(inputTwoDouble);
	}
	public static int methodNoInputReturnInt()
	{
		System.out.println("This method takes in no inputs and returns one int.");
		return 6;
	}
	public static double sumSquareRoot(int NumToSqrtOne, int NumToSqrtTwo)
	{
		System.out.println("This method takes in two inputs, conbines them, and calculates their square root before returning them to the main method where");
		System.out.println("this method was called.");
		int CombNumToSqrt = (NumToSqrtOne + NumToSqrtTwo);
		return Math.sqrt(CombNumToSqrt);
	}	
}