public class AreaComputations
{
	public static double areaSquare(double squareLength)
	{
		double areaOfSquare = (squareLength * squareLength);
		return areaOfSquare;
	}
	public double areaRectangle(double rectangleWidth, double rectangleLength)
	{
		double areaOfRectangle = (rectangleWidth * rectangleLength);
		return areaOfRectangle;
	}
}