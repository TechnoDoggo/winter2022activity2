public class PartThree
{
	public static void main(String args[])
	{
		java.util.Scanner reader = new
		java.util.Scanner(System.in);
		
		System.out.println("Please enter the length of one side of the square.");
		double squareLength = reader.nextDouble();
		
		System.out.println("Please enter the width of the rectangle.");
		double rectangleWidth = reader.nextDouble();
		
		System.out.println("Please enter the length of the rectangle.");
		double rectangleLength = reader.nextDouble();
		
		double squareArea = AreaComputations.areaSquare(squareLength);
		
		AreaComputations Area = new AreaComputations();
		double rectangleArea = Area.areaRectangle(rectangleWidth, rectangleLength);
		
		System.out.println("The area of the square is: " + squareArea + ".");
		System.out.println("The area of the rectangle is: " + rectangleArea + ".");
	}
}